package com.devcamp.task58_40.productlist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58_40.productlist.model.product;

public interface ProductRepository extends JpaRepository<product, Long> {
    
}
